#!/bin/bash

set -e
AAPT="/opt/android-sdk/build-tools/29.0.2/aapt"
DX="/opt/android-sdk/build-tools/29.0.2/dx"
ZIPALIGN="/opt/android-sdk/build-tools/29.0.2/zipalign"
APKSIGNER="/opt/android-sdk/build-tools/29.0.2/apksigner" # /!\ version 26
PLATFORM="/opt/android-sdk/platforms/android-21/android.jar"
APP="referendum"

echo "Cleaning..."
rm -rf obj/*
rm -rf src/com/sysapps/$APP/R.java

echo "Generating R.java file..."
$AAPT package -f -m -J src -M AndroidManifest.xml -S res -I $PLATFORM

echo "Compiling..."
javac -d obj -classpath "./libs/*" -bootclasspath $PLATFORM:libs -sourcepath src src/com/sysapps/$APP/*.java

echo "Translating in Dalvik bytecode..."
$DX --dex --output=classes.dex ./libs/*.jar obj

echo "Making APK..."
$AAPT package -f -m -F bin/$APP.unaligned.apk -M AndroidManifest.xml -S res -I $PLATFORM
$AAPT add bin/$APP.unaligned.apk classes.dex

echo "Aligning and signing APK..."
$ZIPALIGN -f 4 bin/$APP.unaligned.apk bin/$APP.aligned.apk
$APKSIGNER sign --ks referendum.keystore --ks-pass pass:p60mptxy bin/$APP.aligned.apk

echo -n "Test app (y/n) : "
read confirm

if [ "$confirm" == "y" ]; then
	echo "Launching..."
	adb install -r bin/$APP.aligned.apk
	adb shell am start -n com.sysapps.$APP/.MainActivity
fi
