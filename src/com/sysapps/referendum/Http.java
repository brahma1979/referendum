package com.sysapps.referendum;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import java.util.HashMap;
import java.util.Map;
import android.content.Context;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.io.UnsupportedEncodingException;
import org.json.*;

public class Http
{
	final String host="https://sys-apps.com/referendum/";
	boolean is_resp_pend = false;

	MainActivity mact;
	
	public Http(Context context)
	{
		mact = (MainActivity)context;
	}
	
	public static String encodeStr(String url)  
	{
		try
		{
			String encodeURL=URLEncoder.encode( url, "UTF-8" );  
		return encodeURL;  
		} catch (UnsupportedEncodingException e) {  
			return "Issue while encoding" +e.getMessage();  
		}  
      }

	public boolean get(String url, int resp_code)
	{
		return request("GET",url, resp_code);
	}
	private boolean request(String method, String url, int resp_code)
	{
		new Thread()
		{
			public void run()
			{
				is_resp_pend = true;
				StringRequest getRequest = new StringRequest(method.equals("GET")?Request.Method.GET:Request.Method.POST, host+url,
				        new Response.Listener<String>() {
				            @Override
				            public void onResponse(String response) {
				            	is_resp_pend = false;
				            	Util.log(response);
				            	switch(resp_code)
				            	{

				            	}
				            }
				        },
				        new Response.ErrorListener() {
				            @Override
				            public void onErrorResponse(VolleyError error) {
				            	is_resp_pend = false;
				                error.printStackTrace();
//				            	Util.log("RESP ERROR: "+error.toString());
//				            	Util.log("URL "+url);
				            	switch(resp_code)
				            	{
				            		case 1: Util.dispToast("Registration FAILED\n"+error.toString());
Util.log("Registration FAILED\n"+error.toString());
				            				break;
				            		case 2: Util.dispToast("Failed to Withdraw Amount\n"+error.toString());
				            				break;
				            		case 3: Util.dispToast("Failed to Sync. Amount\n"+error.toString());
				            				break;
				            	}
							}
				        }
				) {
					@Override
					public Map<String, String> getHeaders(){
					    Map<String, String> headers = new HashMap<String, String>();
//					    headers.put("User-agent", "Brahma's Update/4.0 Client Android brahma@sys-apps.com");
//				        headers.put("Authorization", "Basic base64-encoded-auth-string");
				        return headers;
					}
					
				    @Override
				    protected Map<String, String> getParams()
				    {
				        Map<String, String>  params = new HashMap<>();
//						        params.put("myip", ip_pub);
				        return params;
				    }
				};
				// Add the request to the RequestQueue.
//				mact.queue.add(getRequest);
			}
		}.start();
		return false;
	}
}

