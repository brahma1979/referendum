package com.sysapps.referendum;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;

public class BD
{
	BluetoothAdapter ba = BluetoothAdapter.getDefaultAdapter();
	private BluetoothSocket bs = null;
	private BluetoothDevice bd = null;
	private boolean is_connecting=false,is_connected=false;//,to_connect=false;
	private String mac_addr = null;
	ConnectingThread ct = new ConnectingThread();
	OutputStream os;
	BufferedReader bis;
	String para_str,valu_str,pair_pin;
	int valu;
	long para,connected_time=0;
	Context c;
	String read_line;
	
	public BD(Context c)
	{
		this.c = c;
		registerReceiver();
	}
	
	@TargetApi(Build.VERSION_CODES.KITKAT)
	protected void registerReceiver()
	{
		c.registerReceiver(br, new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED));
		c.registerReceiver(br, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
		c.registerReceiver(br, new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED));
		c.registerReceiver(br, new IntentFilter(BluetoothDevice.ACTION_PAIRING_REQUEST));
	}
	protected void unregister()
	{
		try
		{
			c.unregisterReceiver(br);
		}
		catch(Exception e){Util.log("Unregisterd when not registered");}
	}
	
	protected void onDestroy()
	{
		try
		{
			c.unregisterReceiver(br);
		}
		catch(Exception e){}
	}
	
	final BroadcastReceiver br = new BroadcastReceiver()
	{
		public void onReceive(Context context, Intent intent)
		{
			if(intent.getAction().equals(BluetoothDevice.ACTION_ACL_DISCONNECTED))
			{
				if(((BluetoothDevice)intent.getExtras().get(BluetoothDevice.EXTRA_DEVICE)).getAddress().equals(mac_addr))
					disconnect();
			}
			else if(intent.getAction().equals(BluetoothDevice.ACTION_ACL_CONNECTED))
			{
				if(((BluetoothDevice)intent.getExtras().get(BluetoothDevice.EXTRA_DEVICE)).getAddress().equals(mac_addr))
					is_connected=true;
			}
			else if(intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED))
			{
				if(intent.getExtras().get(BluetoothAdapter.EXTRA_STATE).equals(BluetoothAdapter.STATE_TURNING_OFF))
					disconnect();
			}
			else if(intent.getAction().equals(BluetoothDevice.ACTION_PAIRING_REQUEST))
			{
				Util.dispToast("Pairing..");
				setBluetoothPairingPin((BluetoothDevice)intent.getExtras().get(BluetoothDevice.EXTRA_DEVICE));
			}
		}
	};
	
	public void setBluetoothPairingPin(BluetoothDevice device)
	{
		if(android.os.Build.VERSION.SDK_INT>=1) //android.os.Build.VERSION_CODES.LOLLIPOP)
		{
			if(this.pair_pin==null) this.pair_pin="1234";
			try
			{
				byte[] pinBytes = (byte[]) BluetoothDevice.class.getMethod("convertPinToBytes", String.class).invoke(BluetoothDevice.class, pair_pin);
				device.getClass().getMethod("setPin", byte[].class).invoke(device, pinBytes);
				device.getClass().getMethod("setPairingConfirmation", boolean.class).invoke(device, true);
				Util.dispToast("Pairing..DONE");
			}
			catch (Exception e) { Util.log("BD "+e.getMessage()); }
		}
	}
	
	protected boolean isConnected()
	{
		return is_connected;
	}
	
	protected String getAddr()
	{
		return mac_addr;
	}
	protected void setAddr(String addr)
	{
		mac_addr = addr;
	}
	protected boolean isConnecting()
	{
		return is_connecting;
	}
	protected void connect()
	{
		if(mac_addr!=null)
			connect(mac_addr);
	}
	
	protected void connect(String addr)
	{
		if(addr==null || is_connecting) return;

		if(is_connected)
			if(!mac_addr.equals(addr))
			{
				mac_addr = addr;
				disconnect();
			}
			else
				return;

		mac_addr = addr;
		is_connecting = true;
		new Thread(ct).start();
	}
	protected void connect(String addr, String pin)
	{
		this.pair_pin = pin;
		connect(addr);
	}

	protected boolean disconnect()
	{
		if(!is_connected) return true;
		
		is_connected = false;
		is_connecting = false;
		connected_time=0;
		try
		{
			if(os!=null) os.close();
			if(bis!=null) bis.close();
			if(bs!=null) bs.close();
			
			os = null;
			bis= null;
			bs = null;
			bd = null;
		}
		catch(Exception e){ Util.log("BS CLOSE ERR "+e.toString()); return false;}
		return true;
	}
	
	protected void finalize()
	{
		disconnect();
	}
	
	protected String readLine()
	{
		if(bs==null || !is_connected) return null;
		
		read_line = null;
		try
		{
			if(bis==null)
				bis = new BufferedReader(new InputStreamReader(bs.getInputStream()),100);
			if(bis.ready())
			{
				read_line = bis.readLine();
			}
		}catch(IOException e){Util.log("BT RX ERR "+e.toString()); return null;}
		return read_line;
	}
	
	protected long get(long p, int timeout) throws TimeoutException
	{
		int count=0;
		
		get(p+"");
		while(timeout/10>count)
		{
			count++;
			Util.sleep(10);
			readPV();
			if(p==para) return valu;
		}
		throw new TimeoutException();
	}
	
	protected long set(long p, long v, int timeout) throws TimeoutException
	{
		int count=0;

		set(p+"",v+"");
		while(timeout/10>count)
		{
			count++;
			Util.sleep(10);
			readPV();
			if(p==para) return valu;
		}
		throw new TimeoutException();
	}
	
	protected void flush()
	{
		while(readLine()!=null){}
	}
	
	protected void readPV()
	{
		para=0; valu=0;
		readLine();
		if(read_line!=null)
		{
			if(read_line.contains("="))
			{
				try
				{
					para = Long.parseLong(read_line.split("=",2)[0].substring(1));
					valu = Integer.parseInt(read_line.split("=",2)[1]);
				}
				catch(Exception e){}
			}
		}
	}
	
	protected boolean hasData()
	{
		return (para!=0) ? true : false;
	}
	
	protected boolean write(String msg)
	{
		if(!is_connected && !is_connecting)
			connect();
Util.log(msg);
		if(bs==null || os==null || !is_connected) return false;
		try
		{
			os.write(msg.getBytes());
			os.write(0);
		}catch(IOException e){Util.log("BT TX ERR "+e.toString()); return false;}
		return true;
	}
	
	protected void set(String para_str, String valu_str)
	{
		flush();
		this.para_str=para_str; this.valu_str=valu_str;
		write("T"+para_str+"="+valu_str);
		try
		{
		    TimeUnit.MILLISECONDS.sleep(150);
		} catch (InterruptedException e){}
	}
	protected void get(String para_str)
	{
		flush(); //to flush
		this.para_str=para_str; this.valu_str="";
		write("T"+para_str);
	}
	protected void baDisable()
	{
		ba.disable();
	}

	class ConnectingThread implements Runnable
	{
		public void run()
		{
			if(!ba.isEnabled())
			{
				ba.enable();
				try { Thread.sleep(3000);} catch(Exception e){}
			}
			if(bd==null)
				bd = ba.getRemoteDevice(mac_addr);

			try
			{
				bs = bd.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
				bs.connect();
				os = bs.getOutputStream();
				is_connected = true;
				is_connecting = false;
			}
			catch(Exception e)
			{
				is_connecting = false;
				disconnect();
				ba.disable();
				try { Thread.sleep(1000);} catch(Exception ex){}
				Util.log(e.toString());
			}
		}
	}
}
