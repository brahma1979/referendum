package com.sysapps.referendum;

import android.util.Log;
import android.widget.Toast;
import android.content.DialogInterface;
import android.widget.EditText;
import android.text.InputType;
import android.content.SharedPreferences;
import android.app.AlertDialog;
import android.content.Context;


final class Util
{
	static MainActivity act_context;
	public static void init(MainActivity cont)
	{
		act_context = (MainActivity)cont;
	}

	public static void sleep(int millis)
	{
		try { Thread.sleep(millis); }catch(Exception e){Log.e("SLEEP ERR","Interrupted from sleep");}
	}
	
	public static void dispToast(String str)
	{
		Toast.makeText(act_context.getApplicationContext(), str, Toast.LENGTH_SHORT).show();
	}
	
	
	public static String toRs(int paise)
	{
		return (paise/100)+"."+(paise%100<10 ? "0"+(paise%100) : (paise%100));
	}
	
	public static void log(String y)
	{
		long time= System.currentTimeMillis();
		Log.e("MYLOGXYZ "+time,y);
	}
	public static void alert(String titl, String message)
	{
		new AlertDialog.Builder(act_context)
	    .setTitle(titl)
    	.setMessage(message)

    // Specifying a listener allows you to take an action before dismissing the dialog.
    // The dialog is automatically dismissed when a dialog button is clicked.
    	.setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) { 
            // Continue with delete operation
        	}
     	})

    // A null listener allows the button to dismiss the dialog and take no further action.
//	    .setPositiveButton(android.R.string.no, null)
	    .setIcon(android.R.drawable.ic_dialog_alert)
	    .show();
	}

	static int dial_func;
	public static String dispDialogue(int func, String message, String pos_btn, String neg_btn)
	{
		String str=null;
		dial_func = func;
		final EditText text_input = new EditText(act_context);
		text_input.setInputType(InputType.TYPE_CLASS_TEXT);
		
		AlertDialog.Builder ad = new AlertDialog.Builder(act_context);
		ad.setMessage(message);
		ad.setView(text_input);

		if(pos_btn!=null)
		{
			ad.setPositiveButton(pos_btn, new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int whichButton)
				{
					SharedPreferences sp = act_context.getPreferences(Context.MODE_PRIVATE);
					SharedPreferences.Editor sp_editor = sp.edit();

					switch(dial_func)
					{
						case 1: break;
					}
				}	
			});
		}
		if(neg_btn!=null)
			ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int whichButton)
				{
				}
			});
		
		ad.show();
		return str;
	}

}


