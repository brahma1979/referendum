#!/bin/bash
# Ask the user for their name
echo Enter old package name :
read opack
echo Enter new package name :
read npack
echo "converting pack from $opack to $npack"
find -regex '.*\.\(java\|xml\|sh\)' -exec sed -i "s/$opack/$npack/g" {} \;
rm bin/*
rm -R gen/*
rm -R obj/*
